#! /bin/sh
# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.
cd "$(dirname "$0")"
./rellink.py ~ \
    .screenrc \
    ~/DOCUMENTS/Gnupg/.gnupg \
    ~/DOCUMENTS/Mutt/.muttrc \
    ~/DOCUMENTS/password-store/.password-store
./rellink.py ~/.vim vimrc
./rellink.py ~/bin bin/*

if [ "$(uname)" = 'Darwin' ]; then
    ./rellink.py ~/bin macos-bin/*
fi

F="$(pwd)/bash_setup.bash"
if [ -f "$F" ]; then
    L=". '${F}'"
    for B in ~/.bashrc ~/.bash_profile; do
        if [ -f "$B" ] && grep -q "^$L$" "$B"; then
            continue
        fi
        printf "\n%s\n" "$L" >> "$B"
    done
fi

./git_configure.sh

mkdir -p ~/tmp/swp
