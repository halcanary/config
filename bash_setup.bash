# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.

command -v dirname > /dev/null || exit

###### FUNCTIONS

    ,pdf_to_png() {
        local A
        local B
        local F
        local G
        local E
        for A; do
            B="$(basename "$A" .pdf)"
            pdfimages -j "$A" "$B"
            if command -v pnmtopng > /dev/null 2>&1; then
                for E in ppm pgm pbm; do
                    for F in "$B"-*."$E"; do
                        if [ -f "$F" ]; then
                            G="$(basename "$F" ."$E").png"
                            pnmtopng -background=white -compression=9 "$F" > "$G" && rm "$F"
                            echo "$G"
                        fi
                    done
                done
            fi
        done
    }

    ,print_path() {
        echo 'PATH='
        echo "$PATH" | tr : \\n | sed 's/^/    /'
        echo
    }

    ,isSubstring() {
        case "$2" in (*"$1"*) return 0;; (*) return 1;; esac
    }

    ,addToPath() {
        local A
        for A; do
            [ -d "$A" ] && ! ,isSubstring ":${A}:" ":${PATH}:" && \
                export PATH="${PATH}:$A";
        done
    }

    ,prependToPath() {
        local A
        for A; do
            [ -d "$A" ] && ! ,isSubstring ":${A}:" ":${PATH}:" && \
                export PATH="${A}:${PATH}";
        done
    }

    ,sourceIfExists() { local A; for A; do [ -f "$A" ] && . "$A"; done; }

    ,sshAdd() {
        if [ "$SSH_AUTH_SOCK" ]; then
            local A
            for A; do
                case "$A" in (*.pub) continue ;; esac
                [ -f "$A" ] && ( ssh-add "$A" 2> /dev/null & )
            done
        else
            echo Enviroment variable SSH_AUTH_SOCK is not set. >&2
        fi
    }

    ,launch_ssh_agent() {
        if [ "$SSH_AUTH_SOCK" ] ; then
            return
        fi
        local E="$(command -v ssh-agent 2> /dev/null)"
        if [ "$?" = 0 ]; then
            eval $($E) 2> /dev/null;
            trap "eval \$(${E} -k) 2> /dev/null" 'EXIT'
        fi
    }

    ,abspath() (
         for A; do
            D="$(dirname "$A")"
            printf %s\\n "$(cd "$D"; pwd)/$(basename "$A")"
         done
    )

    ,absdirname() (
        for A; do
            ( cd "$(dirname "$A")"; pwd; )
        done
    )

    ,resolve_path() (
        B="$(basename "$1")"
        cd "$(dirname "$1")"
        L="$(readlink "$B")"
        if [ "$?" == 0 ]; then
            ,resolve_path "$L"
        else
            echo "$(pwd -P)/$B"
        fi
    )

    ,amp() { ( nohup "$@" > /dev/null 2>&1 & echo $! ); }

    ,browse() {
        if [ "$BROWSER" ]; then
            ,amp "$BROWSER" "$@";
        else
            echo "$@"
        fi
    }

    ,ba() { ,sourceIfExists "${PROFILE_PATH_DIR}/bash_setup.bash"; }

    ,recent() { ls -trlgA "$@" | tail; }

    ,gclone() {
        local A="$(echo "$1" | sed -n 's/^git@gitlab.com://p;s/^git@github.com://p;' | \
            sed 's/\.git$//')"
        [ "$A" ] || { echo bad >&2; return 1; }
        git clone "$1" ~/src/"$A"
        printf "~/src/%s\n" "$A"
    }

    ,term_title() { printf "\033]2;%s\007" "$*"; }

    ,l() {
        ,term_title "$(,abspath "$@" | sed "s|^${HOME}/|~/|" | tr '\n' ' ')";
        if command -v highlight > /dev/null; then
            if [ "$#" -gt 1 ]; then
                highlight --force=txt -O xterm256 --replace-tabs=4 --stdout "$@" \
                    --line-numbers --line-number-length=4 | less -Ri
            else
                if [ -d "$1" ]; then
                    ls -lA --color=always -D '%F %T' "$@" | less -Ri
                else
                    highlight --force=txt -O xterm256 --replace-tabs=4 --stdout "$@" | \
                        less -Ri --line-num-width=4
                fi
            fi
        else
            less -Ri --line-num-width=4 "$@"
        fi
    }

    ltax() {
        HIGHLIGHT_OPTIONS='--out-format=xterm256' highlight --force --syntax="$1" | less -Ri --line-num-width=4
        #highlight --force -O xterm256 --syntax-by-name="$1" | less -RiN --line-num-width=3
    }

    ,vim_git_diff() {
        ( IFS=$'\n'; set -f; exec vim -p $(git diff --name-only --relative "$@") )
    }

    ,data_url() {
        printf '"data:%s;base64,%s"\n' "$(file -b --mime-type "$1")" "$(base64 "$1")"
    }

    ,flasher() { while true; do printf '\a'; sleep 0.25 || break; done; }

    ,runIfExists() { type "$1" > /dev/null 2>&1 && "$1"; }

    ,diary() {
        local F="$("$PROFILE_PATH_DIR"/bin/get_diary_path.sh "$@")"
        printf '~/%s\n' "$F"
        vim ~/"$F" +
    }

    ,diaryPath() { printf '%s\n' ~/"$("$PROFILE_PATH_DIR"/bin/get_diary_path.sh "$@")"; }

    ,bad_exit()  { if [ "$1" != 0 ]; then printf %s $'\033[1;37;41m{'"$1"$'}\033[0m '; fi; }

    ,job_count() { if [ "$1" != 0 ]; then printf %s $'\033[1;32;40m('"$1"$')\033[0m '; fi; }

    ,current_git_branch() {
        local A="$(git symbolic-ref --short HEAD 2> /dev/null)"
        if [ "$A" ]; then printf '\033[1;36m%s\033[0m ' "$A"; fi
        if [ "$(git diff --name-only 2> /dev/null)" ]; then  printf '\033[1;31m✗\033[0m '; fi
    }

    ,isodate() { date '+%Y-%m-%d'; }

    ,isodatetime() { date '+%Y-%m-%d_%H%M%S'; }

    ,prependDate() (
        for A; do
            M="$(stat -f '%m' "$A")"
            mv -v "$A" "$(dirname "$A")/$(date -r "$M" '+%Y-%m-%d')_$(basename "$A")"
        done
    )

    ,unquoteUrl() {
        python3 -c "import sys, urllib.parse; print('\n'.join(urllib.parse.unquote(x) for x in sys.argv[1:]))" "$@"
    }

    ,despacify() {
        local A
        for A; do
            local B="$(basename "$A")"
            local N="$(,unquoteUrl "$B" | sed 's/[^A-Za-z0-9._+,-]/_/g')"
            if [ "$N" != "$B" ]; then
                mv -v "$A" "$(dirname "$A")/$N"
            fi
        done
    }

    ,move_to_trash() {
        local A
        for A; do
            if readlink "$A" > /dev/null; then
                rm "$A"
                continue
            fi
            if ! [ -e "$A" ] ; then
                echo "'$A' does not exist." >&2
                continue
            fi
            local T=~/Trash/"$(,abspath "$A" | sed "s|^${HOME}/|/|" | sed 's|^/||' )"
            mkdir -p "$(dirname "$T")"
            mv -v "$A" "$T"
        done
    }

    ,cmd_exists() { type "$1" > /dev/null 2>&1; }

    ,set_var_if_exists() {
        local A
        local V="$1"
        shift
        for A; do
            if [ -e "$A" ]; then
                eval "${V}='${A}'"
                return
            fi
        done
        return 1
    }

    ,set_export_var_if_exits() {
        local V="$1"
        shift
        if ,set_var_if_exists "$V" "$@" ; then
            export "$V"
        fi
    }

    ,ruler() {
        local I="${1:-80}"
        while [ $(( I -= 1)) -ge 0 ]; do
            printf '-';
        done
        printf '\n'
    }

    ,lazy_file() { "$PROFILE_PATH_DIR"/bin/lazy_file.sh "$@"; }
    ,lazy_less_highlight() { ,l "$(,lazy_file "$1")"; }
    ,lazy_vim() { vim "$(,lazy_file "$1")"; }

    ,ssh_lpr() {
        local DST="$1"
        local ARG
        shift
        for ARG; do
            local BASE="$(basename "$ARG")"
            scp "$ARG" "${DST}:/tmp/$BASE"
            ssh "$DST" lpr "/tmp/$BASE"
        done
    }

    ,fix_permissions() {
        find "${@:-.}" -type d -print0 | xargs -0 chmod u+rwx
    }

    jsontool2() {
        python3 -m json.tool "$@" | highlight --force -O xterm256 --syntax-by-name=json | less -R;
    }

    if [ $(uname) == Darwin ]; then
        ,change() {
            echo "sed -i '' \"s@${1}@${2}@g\" \$(git grep -l \"$1\")"
            if [ "$(git grep -l "$1")" ]; then
                sed -i '' "s@${1}@${2}@g" $(git grep -l "$1")
            fi
        }
        ,replaceall() {
            sed -i '' "s@${1}@${2}@g" $(grep -r -l "$1" "$3")
        }
    else
        ,replaceall() {
            sed -i "s@${1}@${2}@g" $(grep -r -l "$1" "$3")
        }
    fi

    ,scbg() {
        command -v screen > /dev/null && screen -d -m "$@"
    }

    ,long_prompt() {
        export PS1='\[\e]0;\h [\w]\a\]$(,bad_exit $?)$(,job_count \j)$(,current_git_branch)\n[\w]\[\e[1m\]\$\[\e[0m\] '
        LONG_PROMPT=1
    }
    ,regular_prompt() {
        export PS1='\[\e]0;\h [\w]\a\]$(,bad_exit $?)$(,job_count \j)\n[\w]\[\e[1m\]\$\[\e[0m\] '
        LONG_PROMPT=2
    }
    ,short_prompt() {
        export PS1='\[\e[1m\]\$\[\e[0m\] '
        unset LONG_PROMPT
    }

    ,switch_prompt() {
        case "$LONG_PROMPT" in
            ('') ,long_prompt  ;;
            (1)  ,regular_prompt ;;
            (*)  ,short_prompt ;;
        esac
    }

    ,sshAddAll() { ,sshAdd  ~/.ssh/keys/* ; }

    ,logless() (
        RESET=$'\033[0m'
        RED=$'\033[91m'
        GREEN=$'\033[92m'
        YELLOW=$'\033[93m'
        BLUE=$'\033[94m'
        CYAN=$'\033[96m'
        FMT="$BLUE\1$RESET $GREEN\2$RESET $YELLOW\3$RESET +$CYAN\4$RESET $RED\5$RESET "
        PATTERN='^\([^ ]*\) \([^ ]*\) \([^ ]*\):\([^ ]*\): \([^ ]*\) '
        cat "$@" | sed "s@${PATTERN}@${FMT}@"| less -Ri
    )


    ,get_closest_git_release() {
        head="${1:-HEAD}"
        minimum_commit_count=65535
        minimum_commit_ref=''
        for ref in $(git for-each-ref --format='%(refname)' refs/remotes/origin/release/); do
            n="$(git rev-list --count ^"$ref" "$head")"
            if [ "$n" -lt "$minimum_commit_count" ]; then
                minimum_commit_count="$n"
                minimum_commit_ref="$ref"
            fi
        done
        git for-each-ref --format='%(refname:lstrip=4)' "$minimum_commit_ref"
    }
    ,set_up_build_version() {
        export BUILD_VERSION="$(,get_closest_git_release)"
        export BUILD_NUMBER='dev'
        echo "${BUILD_VERSION}-${BUILD_NUMBER}"
    }

###### ENVIRONMENT

    printf '\e[31m%s\e[0m\n' exports
    export CLICOLOR=1
    export BASH_SILENCE_DEPRECATION_WARNING=1
    export CLICOLOR=1
    export EDITOR='vim'
    export HISTCONTROL='ignorespace:ignoredups'
    export HISTFILESIZE=100000
    export HISTSIZE=100000
    export VISUAL='vim'
    export LANG="en_US.UTF-8"

###### SETUP

    printf '\e[31m%s\e[0m\n' prompt
    ,regular_prompt
    HOSTNAME="$(uname -n)"
    KERNEL_NAME="$(uname)"
    PROFILE_PATH_DIR="$(,absdirname "$BASH_SOURCE")"
    export PROMPT_COMMAND='printf "\e]7;%s\a" "file://$HOSTNAME${PWD// /%20}"'

    printf '\e[31m%s\e[0m\n' bash\ completion
    ,sourceIfExists \
        ~/homebrew/etc/profile.d/bash_completion.sh \
        /usr/local/etc/profile.d/bash_completion.sh \
        /usr/share/bash-completion/completions/git

    if shopt -q login_shell && [ "$HOSTNAME" = archimedes ]; then
        printf '\e[31m%s\e[0m\n' ssh
        ,launch_ssh_agent
    fi

    #,prependToPath \
    #    "/Library/Frameworks/Python.framework/Versions/2.7/bin" \
    #,addToPath '/opt/apache-maven/bin' ~/.local/bin
    #,addToPath ~/Applications/CMake.app/Contents/bin
    #,addToPath ~/Library/Python/3.9/bin

    printf '\e[31m%s\e[0m\n' path
    ,addToPath \
        ~/bin \
        ~/homebrew/bin

    printf '\e[31m%s\e[0m\n' sshadd
    ,sshAdd ~/.ssh/keys/*

    printf '\e[31m%s\e[0m\n' browser
    ,set_var_if_exists BROWSER \
        '/usr/bin/firefox' \
        '/usr/bin/google-chrome' \
        '/Applications/Firefox.app/Contents/MacOS/firefox' \
        ~/'Applications/Google Chrome.app/Contents/MacOS/Google Chrome' \
        '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome'

    ,set_var_if_exists APPLE_SDKROOT \
        '/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk'

    if [ "$APPLE_SDKROOT" ]; then
        ,grepmacosheaders() {
            ( cd "$APPLE_SDKROOT"; pwd; grep --color=auto "$1" -r --include '*.h' *; )
        }
    fi

    #,set_export_var_if_exists JAVA_HOME \
    #    "$([ -x /usr/libexec/java_home ] && /usr/libexec/java_home)"

    #,set_export_var_if_exists TESSDATA_PREFIX ~/homebrew/share/tessdata

    case $KERNEL_NAME in
        (Darwin)
            #printf '\e[31m%s\e[0m\n' DEVELOPER_DIR
            #if python3 -c "exit(tuple(int(x) for x in '$(sw_vers -productVersion)'.split('.')) > (12,))"; then
            #    ,set_export_var_if_exits DEVELOPER_DIR /Library/Developer/CommandLineTools
            #fi

            export LSCOLORS=GAGAGAGAxxgagaBABAGAGA
            ,make_open_alias() {
                local A
                local N="$1"
                shift
                for A; do
                    if [ -e "$A" ] ; then
                        alias $N="open -a '$A'"
                        return
                    fi
                done
            }
            ,make_open_alias gimp ~/"Applications/GIMP-2.10.app"
            ;;
        (Linux)
            if command -v systemctl > /dev/null 2>&1; then
                alias restart-display-manager='sudo systemctl restart display-manager'
            fi
            ;;
    esac

###### ALIASES

    printf '\e[31m%s\e[0m\n' aliases
    alias scr='screen -R'
    alias scbb=,scbg
    alias scls='screen -ls'

    alias b=',browse'
    alias chx='chmod +x'
    alias cle='printf \\e[H\\e[2J'
    alias clearback='printf \\e[H\\e[2J\\e[3J'
    alias d=',diary'
    alias dp=',diaryPath'
    alias g='git'
    alias gd='git d'
    alias gst='git status'
    alias jsontool='python3 -m json.tool'
    alias l=',l'
    alias lzf=',lazy_file'
    alias lzlh=',lazy_less_highlight'
    alias lzv=',lazy_vim'
    alias npm-test='CI=true npm test'
    alias pu='ps -u $(id -u -n)'
    alias r="reset; printf '\e[3J'"
    alias recent=',recent'
    alias rms=',move_to_trash'
    alias ssh-lpr=',ssh_lpr'
    alias v='vim -p'
    alias vd=',vim_git_diff'

    printf '\e[31m%s\e[0m\n' __git_complete
    if ,cmd_exists __git_complete; then
         __git_complete g __git_main
    fi
    if [ "$BROWSER" ]; then
        alias b=',browse'
    fi

    if [ "$KERNEL_NAME" == Darwin ]; then
        alias o='open'
    elif command -v xdg-open > /dev/null 2>&1; then
        alias o='xdg-open'
    fi
    alias r="reset; printf '\e[3J'"
    alias lzlh=',lazy_less_highlight'
    alias ssh-lpr=',ssh_lpr'

    alias lzv=',lazy_vim'
    alias lzf=',lazy_file'
    alias pp=',switch_prompt'
    alias ssh-add-all=',sshAddAll'

,print_path
