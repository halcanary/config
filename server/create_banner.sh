#! /bin/sh

##  Usage:
##      ./create_banner.sh | sudo tee /etc/banner.txt
##      echo 'Banner /etc/banner.txt' | sudo tee -a /etc/ssh/sshd_config

cat << EOF
********************************************************************************
                         !!!!!!  W A R N I N G  !!!!!!
                       THIS IS A PRIVATE COMPUTER SYSTEM
                         UNAUTHORIZED ACCESS PROHIBITED
                             AUTHORIZED ACCESS ONLY
********************************************************************************

EOF

for file in /etc/ssh/ssh_host_*_key.pub; do
    if [ -e "$file" ]; then
        ssh-keygen -lf "$file";
    fi
done | sed 's/ \S*@\S*//' | column -t | sed 's/^/    /'

cat << EOF

********************************************************************************
EOF
