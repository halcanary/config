#! /bin/sh

if [ $(id -u) != 0 ]; then
    printf "Usage:\n  sudo %s\n" >&2
    exit 1
fi

D="$(dirname "$0")"

cp "$D"/00-update-motd /etc/update-motd.d/00-update-motd
chmod +x /etc/update-motd.d/00-update-motd

sh "$D"/create_banner.sh > /etc/banner.txt
echo 'Banner /etc/banner.txt' >> /etc/ssh/sshd_config
