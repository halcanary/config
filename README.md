# Hal's Config

Copyright 2020 Hal W Canary III

ALL RIGHTS RESERVED.

* * *

## Hal's installation instructions:

Generate and add ssh key:

```
mkdir -p ~/.ssh/keys
ssh-keygen -P '' -f ~/.ssh/keys/"$(uname -n)"
[ "$SSH_AUTH_SOCK" ] || eval $(ssh-agent);
ssh-add ~/.ssh/keys/"$(uname -n)"
cat ~/.ssh/keys/"$(uname -n).pub"
```

Install key in <https://gitlab.com/-/profile/keys>.

(Also eventually in <https://github.com/settings/keys>.)

(Also, possibly append to `~/.ssh/authorized_keys` on home server.)

```
ssh USER@HOSTNAME tee -a .ssh/authorized_keys < ~/.ssh/keys/"$(uname -n).pub"
```

Clone this repository:

```
git --version
git clone git@gitlab.com:halcanary/config.git \
    ~/src/halcanary/config
~/src/halcanary/config/setup.sh
. ~/.bashrc
```

Grab notes:

```
git clone git@gitlab.com:halcanary/notes.git ~/src/halcanary/notes
(cd ~; ln -s src/halcanary/notes/Notes .;)
```

### MacOS:

```
chsh -s /bin/bash
xcode-select --install
~/src/halcanary/config/macos.sh
```

* * *

![](./HAL_CANARY_20191008_512.jpg)
