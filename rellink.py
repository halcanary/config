#! /usr/bin/env python3
# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.

import os
import sys

'''
Example use:
    rellink.py /usr/local/bin /usr/local/go/bin/*
'''

def rellink(dst, srcs):
    assert os.path.isdir(dst) or not os.path.exists(dst)
    if not os.path.exists(dst):
        os.makedirs(dst)
    dst = os.path.realpath(dst)
    for src in srcs:
        if not os.path.exists(src):
            sys.stderr.write('path "%s" does not exist.\n' % src)
            continue
        newpath = os.path.join(dst, os.path.basename(src))
        if os.path.exists(newpath):
            if os.path.realpath(newpath) != os.path.realpath(src):
                sys.stdout.write('path "%s" already exists.\n' % newpath)
            continue
        os.symlink(os.path.relpath(src, dst), newpath)

if __name__ == '__main__':
    rellink(sys.argv[1], sys.argv[2:])
