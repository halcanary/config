" " Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.

set expandtab tabstop=4 shiftwidth=4 ruler
set laststatus=2
set directory=$HOME/tmp/swp//
set hlsearch
set ignorecase smartcase
set visualbell
set wrap linebreak
set nofixendofline
set shellredir=>
set path=$PWD/**
set title
syntax on
nnoremap gf <C-W>gf
vnoremap gf <C-W>gf
if system("uname") == "Darwin\n"
    vmap <C-x> :!pbcopy<CR>
    vmap <C-c> :w !pbcopy<CR><CR>
    command! V :r !pbpaste
    command! P :r !pbpaste
elseif has('clipboard')
    vnoremap <C-c> "+y
    vnoremap <C-x> "+d
    command! V normal! "*p
    command! P normal! "*p
else
    vnoremap <C-c> :w !xclip -selection clipboard<CR><CR>
    vnoremap <C-x> :!xclip -selection clipboard<CR>
    command! V :r !xclip -o -selection clipboard
    command! P :r !xclip -o -selection clipboard
endif
nnoremap <C-S-tab> :tabprevious<CR>
nnoremap <C-tab>   :tabnext<CR>
nnoremap <C-L> :nohl<CR><C-L>

vmap ,t :!column -t \| sed 's/ \(  *\)/\1/g'<CR>
vmap ,s :'{,'}sort<CR>

" Commenting blocks of code.
augroup commenting_blocks_of_code
  autocmd!
  autocmd FileType swift,c,cpp,java,scala,go       let b:comment_leader = '// '
  autocmd FileType sh,ruby,python,conf,fstab,zsh   let b:comment_leader = '# '
  autocmd FileType tex                       let b:comment_leader = '% '
  autocmd FileType mail                      let b:comment_leader = '> '
augroup END
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

command! W update
command! Spell set spell!

function! Guard()
    let l:guard = toupper(expand('%:gs/[^0-9a-zA-Z_]/_/g'))
    call append(0, "#ifndef " . l:guard)
    call append(1, "#define " . l:guard)
    call append("$", "#endif  // " . l:guard)
endfunction
command! Guard call Guard()

function! Filter(cmd)
    let l:line = line(".")
    execute "%!" . a:cmd
    if v:shell_error
        undo
    endif
    execute "normal! " . l:line . "G zz"
endfunction

function! Fence() range
    :'<,'>s/^    //
    call append(line("'<")-1, '```')
    call append(line("'>"), '```')
endfunction
command! -range Fence call Fence()

command! Gofmt call Filter("gofmt")

command! Pep8 call Filter("python3 -m autopep8 -")

"" autocmd BufWritePre *.go call Filter("gofmt")

autocmd BufWritePost *.go call system("gofmt -w " . expand('%')) | silent edit!

command! Cfmt call Filter("clang-format -style=file")

au BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4

command! Log :normal iconsole.log(JSON.stringify({}, null, 2));<ESC>

command! Dtws :%s/\s\+$// | nohl

""autocmd FileType cpp,c,cxx,h,hpp,python,sh,java setlocal colorcolumn=101
autocmd FileType cpp,c,cxx,h,hpp,python,sh,java match ErrorMsg '\%>100v.\+'

autocmd FileType make setlocal noexpandtab
autocmd FileType java setlocal suffixesadd=.java
autocmd FileType java setlocal includeexpr=substitute(v:fname,'\\.','/','g')

autocmd BufNewFile,BufRead *.mm set filetype=objc

let ConfigPath = fnamemodify(resolve(expand('<sfile>:p')), ':p:h')
command! -nargs=? D execute "tab split " . '~/' . system(ConfigPath . "/bin/get_diary_path.sh " . <q-args>)
command! -nargs=1 LF execute "tab split " system(ConfigPath . "/bin/lazy_file.sh " . <q-args>)

command! Chx update % | execute 'silent !chmod +x %' | e % | execute ':redraw!'
command! ClangFormat update % | execute 'silent ! clang-format -i --style=file %' | e % | execute ':redraw!'

command! Source update % | source %
command! Sort :'{,'}SORT

command! U tabdo update

command! -range SnakeU '<,'> s/\([a-z]\)\([A-Z]\)/\1_\2/g | '<,'> s/.*/\U\0/g | nohl
command! -range SnakeL '<,'> s/\([a-z]\)\([A-Z]\)/\1_\2/g | '<,'> s/.*/\L\0/g | nohl

nohl

if has("wildmenu")
    set wildmenu
    set wildmode=longest,list
endif
set smartindent

com! CheckHighlightUnderCursor echo {l,c,n ->
        \   'hi<'    . synIDattr(synID(l, c, 1), n)             . '> '
        \  .'trans<' . synIDattr(synID(l, c, 0), n)             . '> '
        \  .'lo<'    . synIDattr(synIDtrans(synID(l, c, 1)), n) . '> '
        \ }(line("."), col("."), "name")

syntax on
syntax enable
highlight Error term=Bold cterm=NONE ctermfg=Red ctermbg=NONE gui=NONE guifg=Red guibg=NONE
highlight markdownError term=Bold cterm=NONE ctermfg=Red ctermbg=NONE gui=NONE guifg=Red guibg=NONE

