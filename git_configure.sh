#! /bin/sh
# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.

if ! command -v git > /dev/null 2>&1; then
    echo 'git not found' >&2
    exit 1
fi

git config --global user.name  'Hal Canary'
git config --global user.email 'halcanary@gmail.com'

git config --global advice.detachedHead 'false'
git config --global init.defaultBranch  'main'

git config --global pretty.lg   '%C(green)%h%C(yellow) %ae%C(reset)%C(red)%d%C(reset)%n%s'
git config --global pretty.logg  \
'format:[commit]    %C(yellow)%H%C(reset)%C(auto)%d%C(reset)%n[tree]      %T%n[parent]    %P%n[author]    %C(green)%an <%ae>%C(reset) %C(cyan)%ad%C(reset)%n[committer] %C(green)%cn <%ce>%C(reset) %C(cyan)%cd%C(reset)%n%n%C(yellow)%B%n'

git config --global alias.b           'branch'
git config --global alias.s           'status'
git config --global alias.ss          'status --short'
git config --global alias.ch          'checkout'
git config --global alias.detach      'checkout --detach'
git config --global alias.lg          'log    --graph --format=lg'
git config --global alias.logg        'log    --graph --format=logg --numstat'
git config --global alias.ah          'commit --amend --no-edit'
git config --global alias.wip         'commit --no-verify --message=WIP'
git config --global alias.unmerged    'diff --name-only --diff-filter=U'
git config --global alias.d           'diff --name-only'
git config --global alias.ds          'diff --stat'
git config --global alias.dss         'diff --shortstat'
git config --global alias.pshf        'push --force'
git config --global alias.head-branch 'symbolic-ref --short HEAD'
git config --global alias.lsremote    'for-each-ref "--format=%(refname:short)" refs/remotes'
git config --global alias.getuser     'log -1 --format=%al'

git config --global alias.frh  \
    '!f() { R="${1:-origin}"; if git fetch "$R"; then git rebase "$R/${2:-HEAD}" || git rebase --abort; fi; }; f'

git config --global alias.delete-this-branch \
    '! B=$(git symbolic-ref --short HEAD) && git checkout -q --detach && git branch --delete $B'

git config --global alias.make-branch-name \
    "!git log -1 --format=%s HEAD | sed 's@/@ @g;s@\] *\[@/@g;s@^\[\([^]]*\)] *@\1/@;s@[^a-zA-Z0-9/-]\{1,\}@_@g'"

git config --global alias.make-branch    \
    '!f() ( X="$1"; while ! git checkout -b "$X"; do X="${1}_$((++j))"; done; ); f'

git config --global alias.feature-branch '!git make-branch feature/$(git getuser)/$(git make-branch-name)'
git config --global alias.bugfix-branch  '!git make-branch  bugfix/$(git getuser)/$(git make-branch-name)'
git config --global alias.hotfix-branch  '!git make-branch  hotfix/$(git getuser)/$(git make-branch-name)'
git config --global alias.mb             '!git checkout -b "$(git log -1 --format=%f HEAD)"'
git config --global alias.wip-branch     '!git checkout -b "WIP/$(date +%Y-%m-%d_%H%M%S)"'

git config --global alias.pushup \
    '!git push --set-upstream origin "$(git symbolic-ref --short HEAD)"'
