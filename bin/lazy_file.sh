#!/bin/sh
[ "$1" ] || exit
nonempty() ( X="$(tee)"; [ "$X" ] && echo "$X"; )
if git status > /dev/null 2>&1; then
    for p in "$1" "*/$1" "${1}.*" "*/${1}.*" "${1}*" "*/${1}*" "*${1}.*" "*${1}*"; do
        git ls-files "$p" | head -n 1 | nonempty && exit
    done
fi
for p in "$1" "${1}.*" "${1}*" "*${1}.*" "*${1}*"; do
    find . -type f -iname "$p" | head -n 1 | nonempty && exit
done
exit 1
