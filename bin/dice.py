#! /usr/bin/env python3
# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.

import os
import random
import re

def append_decimals(decimals):
    v = 0
    for d in decimals:
        v = v * 10 + d
    return v

def getwords(filepath):
    if os.path.exists(filepath):
        with open(filepath) as f:
            r = re.compile('^[1-6]{5}\t')
            return {int(k): v for k, v in (l.split() for l in f if r.match(l))}

def makepass(values, words):
    return ' '.join(words[v] for v in values) if words else repr(values)

def main():
    DICE_PATH = os.path.expanduser('~/Downloads/diceware.wordlist.asc.txt')
    SYSTEM_RAND = random.SystemRandom()
    print(makepass([append_decimals((SYSTEM_RAND.randint(1, 6)
                                     for _ in range(5)))
                    for _ in range(6)],
                   getwords(DICE_PATH)))

if __name__ == '__main__':
    main()
