#! /usr/bin/env python3
# similar to sort | uniq -c | sort -n
import sys
lines = {}
for line in sys.stdin:
    line = line.rstrip('\n')
    lines[line] = 1 + lines.get(line, 0)
for n, l in sorted((n, l) for l, n in lines.items()):
    print('%d\t%s' % (n, l))
