#! /bin/sh
if ! command -v pdftk > /dev/null; then
    echo "pdftk not found" >&2
    exit 1
elif ! [ -f "$1" -a -f "$2" -a "$3" ]; then
    echo "Usage:  $0 FRONT_PAGES BACK_PAGES OUTPUT_FILE" >&2
    exit 1
fi
pdftk A="$1" B="$2" shuffle output "$3"
