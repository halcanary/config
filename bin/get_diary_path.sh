#!/bin/bash
# Copyright 2020 Hal W Canary III. ALL RIGHTS RESERVED.
C='//'
PREFIX=''
EXE=''
UC=''
case "$1" in
    (sh)
        EXE='true'
        PREFIX='#! /bin/sh'
        C='#';;
    (zsh)
        EXE='true'
        PREFIX='#! /bin/zsh'
        C='#';;
    (py)
        EXE='true'
        PREFIX='#! /usr/bin/env python3'
        C='#';;
    (bash)
        EXE='true'
        PREFIX='#! /bin/bash'
        C='#';;
    (c)
        PREFIX=$'#include <stdio.h>\nint main(int argc, char** argv) {\n  puts("helloworld");\n}\n'
        ;;
    (cpp)
        PREFIX=$'#include <cstdio>\nint main(int argc, char** argv) {\n  std::puts("helloworld");\n}\n'
        ;;
    (md)
        C='<!-- '
        UC=' -->'
        ;;
esac
CURRENT=$(date '+%Y/%m')
F="Notes/$(date +'%Y/%m/notes-%Y-%m-%d_%a').${1:-txt}"
mkdir -p "$(dirname ~/"$F")"

[ -h ~/Notes/cur ] && [ "$(readlink ~/Notes/cur)" != $CURRENT ] && rm ~/Notes/cur

[ -e ~/Notes/cur ] || ln -s $CURRENT ~/Notes/cur

[ -f ~/"$F" ] || printf '%s\n%s ~/%s%s\n\n' "$PREFIX" "$C" "$F" "$UC" >> ~/"$F"
[ "$EXE" ] && chmod +x ~/"$F"
printf %s "$F"
