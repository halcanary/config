#! /usr/bin/env python3
# Copyright 2021 Hal W Canary, III. All rights reserved.
import subprocess, sys
if sys.platform == 'darwin':
  subprocess.check_call(['pstree', '-g3', '-s', 'login'])
  sys.exit(0)
if not sys.platform.startswith('linux'):
  sys.stderr.write(':(\n')
  sys.exit(1)
process_parent_map = dict()
cmd = ['ps', '-o', 'pid=,ppid=']
for c in ['sshd', 'gnome-terminal-server', 'screen', 'login', 'sudo']:
    cmd += ['-C', c]
for line in subprocess.check_output(cmd).splitlines():
  line = line.strip()
  if line:
    pid, ppid = line.split()
    process_parent_map[pid] = ppid
for pid, parent in process_parent_map.items():
  if parent not in process_parent_map:
    # this process doesn't have a parent in the map
    subprocess.check_call(['pstree', '-h', '-p', pid])
