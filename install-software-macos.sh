#! /bin/sh

# Copyright 2022 Hal W Canary III. ALL RIGHTS RESERVED.

#  Install useful software on a new MacOS machine:
#
#  1.  Install Python3 from <https://python.org>
#
#  2.  Install Xcode, Outlook, Slack from MacOS App Store.
#
#  3.  Install or upgrade Homebrew.
#
#  4.  Use Homebrew to install google-chrome, inkscape, gimp, firefox
#      Applications.
#
#  5.  Use Homebrew to install other packages.
#
#  6.  Install Zoom from <https://zoom.us>
#
#  Each step is skipped if it is not needed.
#
#  More software can be added by editing this script.

################################################################################

BREW_PREFIX=~/homebrew

set -x -e
test "$(uname)" = 'Darwin'
renice -n 5 -p $$

################################################################################

command -v python3 > /dev/null 2>&1 || open -W "https://www.python.org/downloads"

################################################################################

openAppStore() {
    open -W -a "/System/Applications/App Store.app" "https://apps.apple.com/us/app/$1"
}

# The app ID can be retrieved by selecting "Copy Link" from the app store.

[ -e "/Applications/Xcode.app"             ] || openAppStore xcode/id497799835
[ -e "/Applications/Microsoft Outlook.app" ] || openAppStore microsoft-outlook/id985367838
[ -e "/Applications/Slack.app"             ] || openAppStore slack-for-desktop/id803453959
[ -e "/Applications/Microsoft Remote Desktop.app" ] || openAppStore microsoft-remote-desktop/id1295203466

################################################################################

installFormula() {
  for A; do
    [ -e "${BREW_PREFIX}/Cellar/$A" ] || brew install "$A"
  done
}

installCask() {
  [ -e "${BREW_PREFIX}/Caskroom/$1" ] || brew install --cask "$1"
}

if ! [ -e "$BREW_PREFIX" ]; then
  mkdir -p "$BREW_PREFIX"
  curl -L https://github.com/Homebrew/brew/tarball/master | \
    tar xz --strip 1 -C "$BREW_PREFIX"
fi

export PATH="${BREW_PREFIX}/bin:${PATH}"

brew update
brew upgrade
brew tap homebrew/cask
brew upgrade --cask

installFormula    \
  clang-format    \
  git             \
  highlight       \
  pandoc          \
  pstree          \
  pwgen           \
  tesseract       \
  cmake           \
  go              \
  ffmpeg          \
  pass            \
  bash-completion \
  ghostscript     \
  gnupg           \
  imagemagick     \
  gnu-sed

[ -e "/Applications/Google Chrome.app" ] || installCask google-chrome
[ -e "/Applications/Inkscape.app"      ] || installCask inkscape
[ -e "/Applications/GIMP.app"          ] || installCask gimp
[ -e "/Applications/Firefox.app"       ] || installCask firefox

[ -e "/Applications/zoom.us.app" ] || open -W "https://zoom.us/download"
