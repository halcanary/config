#! /bin/sh
# Copyright 2022 Hal W Canary III. ALL RIGHTS RESERVED.
mkdir -p ~/.ssh
cat >> ~/.ssh/config << EOF 
Host halcanaryL
	Hostname 192.168.86.100
	User     halcanary
Host halcanary
	Hostname halcanary.duckdns.org
	User     halcanary
EOF
