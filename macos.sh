#! /bin/sh

set -x -e

test "$(uname)" = 'Darwin'

touch ~/.bash_sessions_disable

"$(dirname "$0")/install-software-macos.sh"

"$(dirname "$0")/setup.sh"
